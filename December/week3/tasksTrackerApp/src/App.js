import React,{Component} from 'react'
import Navigator from './components/Navigator'
import store from './store'
import {Provider} from 'react-redux'

export default class App extends Component {
    render(){
        return(
            <Provider store={store}>
                <Navigator/>
            </Provider>
        )
    }
}