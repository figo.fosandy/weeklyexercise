import React,{Component} from 'react'
import {View,Text,TextInput,TouchableOpacity,FlatList,Image} from 'react-native'
import {styles} from '../styles'
import DatePicker from 'react-native-datepicker'
import {connect} from 'react-redux'
import {actionTask,updateTask,addTask} from '../actions'

class TaskDetail extends Component{
    static navigationOptions={
        header:null
    }

    constructor(props){
        super(props)
        const {getParam}=this.props.navigation
        const payload=getParam('payload')||{}
        this.state={
            action:getParam('action'),
            id:payload.id||null,
            detail:{
                title:payload.title||null,
                description:payload.description||null,
                dueDate:payload.dueDate?payload.dueDate.slice(0,10):new Date().toISOString().slice(0,10),
                comments:[]
            },
            timeLine:{
                startedAt:payload.startedAt?new Date(payload.startedAt).toLocaleString():'Not Started Yet',
                finishAt:payload.finishAt?new Date(payload.finishAt).toLocaleString():'Not Finish Yet',
            },
            status:payload.status||null,
            baseDate:getParam('baseDate'),
            allComments:payload.comments||[]
        }
        if(this.state.action=='create'){
            Object.assign(this.state,{
                Title:true,
                Description:true,
                changeDueDate:true,
                addComments:true
            })
        }
    }

    hideComponent(component){
        this.setState({[component]:!this.state[component]})
    }

    captionText(field){
        return (<Text style={[styles.baseText,styles.captionText]}>{field} :</Text>)
    }

    changeStringValue(field){
        if(!this.state[field]){
            return(
                <Text
                    style={[styles.baseText,styles.detailContent]}
                    onPress={()=>this.hideComponent(field)}
                >
                    {this.state.detail[field.toLowerCase()]}
                </Text>
            )
        }
        return(
            <TextInput
                style={[styles.baseText,styles.detailContent]}
                value={this.state.detail[field.toLowerCase()]}
                placeholderTextColor='orangered'
                placeholder={`Input The ${field}`}
                onChangeText={(text)=>this.setState({detail:{
                    ...this.state.detail,
                    [field.toLowerCase()]:text
                }})}
                onEndEditing={()=>{
                    if(/^[a-zA-Z]+(\s*[a-zA-Z])*$/.test(this.state.detail[field.toLowerCase()].trim())){
                        this.setState({
                            detail:{
                                ...this.state.detail,
                                [field.toLowerCase()]:this.state.detail[field.toLowerCase()].trim()
                            }
                        })
                        this.hideComponent(field)
                    } else {
                        alert(`${field} must be alphabetic`)
                        this.setState({
                            detail:{
                                ...this.state.detail,
                                [field.toLowerCase()]:this.state.detail[field.toLowerCase()].trim().replace(/[^a-zA-Z\s]/g,'')
                            }
                        })
                    }
                }}
            />
        )
    }

    changeDueDate(){
        if(this.state.changeDueDate){
            return (
                <DatePicker
                    style={[styles.datePickerSize,styles.detailContent]}
                    date={this.state.detail.dueDate}
                    onDateChange={date=>{
                        if(new Date(date)>=new Date(new Date().toISOString().slice(0,10))) {
                            this.hideComponent('changeDueDate')
                            this.setState({detail:{...this.state.detail,dueDate:date}})
                        } else {
                            alert('Invalid Date')
                            this.setState({detail:{...this.state.detail,dueDate:new Date().toISOString().slice(0,10)}})
                        }
                    }}
                />
            )
        }
        return (
            <Text 
                style={[styles.baseText,styles.detailContent]}
                onPress={()=>this.hideComponent('changeDueDate')}
            >{this.state.detail.dueDate}</Text>
        )
    }

    addComments(){
        if(this.state.addComments){
            const newComments=this.state.detail.comments
            const allComments=this.state.allComments
            return(
                <TextInput
                    style={[styles.baseText,styles.detailContent]}
                    placeholderTextColor='orangered'
                    placeholder='input new comments'
                    onEndEditing={(e)=>{
                        if(/^[a-zA-Z]+(\s*[a-zA-Z])*$/.test(e.nativeEvent.text.trim())){
                            newComments.push(e.nativeEvent.text.trim())
                            allComments.push(e.nativeEvent.text.trim())
                            this.setState({detail:{...this.state.detail,comments:newComments},allComments:allComments})
                            this.hideComponent('addComments')
                        } else {
                            alert(`Comments must be alphabetic`)
                            e.nativeEvent.text=e.nativeEvent.text.replace(/[^a-zA-Z\s]/g,'')
                        }
                    }}
                />
            )
        }
    }

    showTitle(action){
        if(action=='update'){
            return (
                <Text
                    style={[styles.baseText,styles.detailContent]}
                >{this.state.detail.title}</Text>
            )
        }
        return this.changeStringValue('Title')
    }

    showTaskAction(){
        if(this.state.status!='finish'){
            const action=this.state.status=='new'?'Start':'Finish'
            if(!this.state[this.state.status]) {
                return(
                    <TouchableOpacity
                        style={styles.rowDirection}
                        onPress={()=>{
                            this.hideComponent(this.state.status)
                            this.setState({taskAction:action.toLocaleLowerCase()})
                        }}
                    >
                        <Text style={[styles.baseText,styles.captionText]}>{action} The Task</Text>
                        <Image
                            style={styles.actionIconSize}
                            source={action=='Start'?require('../icon/start.png'):require('../icon/finish.png')}
                        />
                    </TouchableOpacity>
                )
            }
            return(
                <View style={styles.rowDirection}>
                    <Text style={[styles.baseText,styles.captionText]}>The Task will {`${action}ing`}</Text>
                </View>
            )
        }
    }

    showTimeLine(action){
        if(action=='update'){
            return (
                <View style={styles.baseFlex}>
                    {this.captionText('Status')}
                    <Text
                        style={[styles.baseText,styles.detailContent]}
                    >{this.state.status}</Text>
                    {this.captionText('Started At')}
                    <Text
                        style={[styles.baseText,styles.detailContent]}
                    >{this.state.timeLine.startedAt}</Text>
                    {this.captionText('Finish At')}
                    <Text
                        style={[styles.baseText,styles.detailContent]}
                    >{this.state.timeLine.finishAt}</Text>
                    {this.showTaskAction()}
                </View>
            )
        }
    }

    display(action){
        return(
            <View style={[styles.content,styles.caption,{flexDirection:'row'}]}>
                <View style={styles.baseFlex}>
                    {this.captionText('Title')}
                    {this.showTitle(action)}
                    {this.captionText('Description')}
                    {this.changeStringValue('Description')}
                    {this.captionText('Due Date')}
                    {this.changeDueDate()}
                    <Text
                        style={[styles.baseText,styles.captionText]}
                        onPress={()=>this.hideComponent('addComments')}
                        >Comments :</Text>
                    {this.addComments()}
                    <FlatList
                        data={this.state.allComments}
                        style={styles.detailContent}
                        keyExtractor={(item,index)=>index.toString()}
                        renderItem={({item})=>{
                            return(
                                <Text style={styles.baseText}>{item}</Text>
                                )
                            }}
                            />
                </View>
                {this.showTimeLine(action)}
            </View>
        )
    }

    render(){
        return(
            <View style={styles.baseBackground}>
                <View style={styles.header}>
                    <Text style={[styles.baseText,styles.headerText]}>Task Detail</Text>
                    <TouchableOpacity
                        onPress={()=>this.props.navigation.goBack()}
                    >
                        <Image
                            style={styles.iconSize}
                            source={require('../icon/home.png')}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.body}>
                    {this.display(this.state.action)}
                </View>
            </View>
        )
    }

    componentWillUnmount(){
        if(this.state.action=='update'){
            if(this.state.taskAction){
                this.props.actionTask(this.state.id,this.state.taskAction,this.state.baseDate)
            }
            this.props.updateTask(this.state.id,this.state.detail,this.state.baseDate)
        } else {
            this.props.addTask(this.state.detail,this.state.baseDate)
        }
    }
}

const mapStateToProps=state=>{
    return {
        task:state.task
    }
}

export default connect(
    mapStateToProps,
    {actionTask,updateTask,addTask}
)(TaskDetail)