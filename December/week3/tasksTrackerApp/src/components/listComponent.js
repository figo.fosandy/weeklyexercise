import React,{Component} from 'react'
import {TouchableOpacity,Text} from 'react-native'
import {styles} from '../styles'

export const List=(props,navigation)=>{
    return (
        <TouchableOpacity
            style={[styles.content,styles.caption,styles.detail]}
            onPress={()=>navigation.navigate('TaskDetail',{action:'update',payload:props,baseDate:props.dueDate})}
        >
            <Text style={styles.baseText}>Title : {props.title}</Text>
            <Text style={styles.baseText}>Description : {props.description}</Text>
        </TouchableOpacity>
    )
}