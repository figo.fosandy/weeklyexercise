import React,{Component} from 'react'
import {View,Text,Image,TouchableOpacity,FlatList} from 'react-native'
import {styles} from '../styles'
import DatePicker from 'react-native-datepicker'
import {List} from './listComponent'
import {connect} from 'react-redux'
import {getTaskList} from '../actions'

class TaskList extends Component {
    static navigationOptions={
        header:null
    }

    constructor(props){
        super(props)
        this.state={
            dueDate:new Date().toISOString().slice(0,10)
        }
    }
    
    getList(){
        this.props.getTaskList(this.state.dueDate,'new')
        this.props.getTaskList(this.state.dueDate,'in-progress')
        this.props.getTaskList(this.state.dueDate,'finish')        
    }
    
    componentDidMount(){
        this.getList()
    }
    
    hideComponent(component){
        this.setState({[component]:!this.state[component]})
    }
    
    changeDueDate(){
        if(this.state.changeDueDate){
            return (
                <DatePicker
                    style={styles.datePickerSize}
                    date={this.state.dueDate}
                    onDateChange={date=>{
                        this.hideComponent('changeDueDate')
                        this.setState({dueDate:date})
                        this.getList()
                    }}
                />
            )
        }
        return (
            <Text 
                style={[styles.baseText,styles.captionText]}
                onPress={()=>this.hideComponent('changeDueDate')}
            >Tasks for {this.state.dueDate==new Date().toISOString().slice(0,10)?'Today':this.state.dueDate}</Text>            
        )
    }
    
    loadingTasks(tasks){
        if(this.props.task[`${tasks.toLowerCase()}Loading`]){
            return(
                <Text style={styles.baseText}>Loading ...</Text>
            )
        } else {
            if(this.props.task[`${tasks.toLowerCase()}List`].length){
                return(
                    <FlatList
                        data={this.props.task[`${tasks.toLowerCase()}List`]}
                        renderItem={({item})=>List(item,this.props.navigation)}
                        keyExtractor={(item)=>item.title}

                    />
                )
            }
        }
        return(
            <Text style={styles.baseText}>You don't have any {tasks} task</Text>
        )
    }
    
    hideTasks(tasks){
        if(!this.state[tasks]){
            return(
            <View style={styles.content}>
                <Text 
                    style={styles.baseText}
                    onPress={()=>{
                        this.hideComponent('New')
                        this.hideComponent('In-progress')
                        this.hideComponent('Finish')
                        this.setState({[tasks]:false})
                    }}
                >{tasks} Tasks</Text>
                {this.loadingTasks(tasks)}
            </View>
            )
        }
    }
    
    render(){
        return(
            <View style={styles.baseBackground}>
                <View style={styles.header}>
                    <Text style={[styles.baseText,styles.headerText]}>Task List</Text>
                    <TouchableOpacity
                        onPress={()=>this.props.navigation.navigate('TaskDetail',{action:'create',baseDate:this.state.dueDate})}
                    >
                        <Image
                            style={styles.iconSize}
                            source={require('../icon/add.png')}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.body}>
                    <View style={[styles.content,styles.caption]}>
                        {this.changeDueDate()}
                    </View>
                    {this.hideTasks('New')}
                    {this.hideTasks('In-progress')}
                    {this.hideTasks('Finish')}
                </View>
            </View>
        )
    }
}

const mapStateToProps=state=>{
    return {
        task:state.task
    }
}

export default connect(
    mapStateToProps,
    {getTaskList}
)(TaskList)