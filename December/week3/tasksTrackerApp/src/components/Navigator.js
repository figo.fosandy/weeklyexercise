import {createAppContainer} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import TaskList from './TaskList'
import TaskDetail from './TaskDetail'

const Navigator=createStackNavigator({
    TaskList:{screen:TaskList},
    TaskDetail:{screen:TaskDetail}
})

export default createAppContainer(Navigator)