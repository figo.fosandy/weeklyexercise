import {StyleSheet} from 'react-native'

export const styles=StyleSheet.create({
    baseBackground:{
        backgroundColor:'black'
    },
    baseText:{
        color:'red'
    },
    header:{
        position:'absolute',
        height:60,
        width:'100%',
        alignItems:'center',
        justifyContent:'center',
        borderBottomWidth:4,
        borderRightWidth:4,
        borderBottomRightRadius:30,
        borderColor:'red',
        flexDirection:'row'
    },
    body:{
        marginTop:60,
        height:'100%'
    },
    headerText:{
        fontWeight:'bold',
        fontSize:30
    },
    content:{
        flex:1,
        borderWidth:3,
        borderColor:'red',
        padding:10,
        margin:5,
        marginBottom:0,
        borderTopLeftRadius:25,
        borderBottomRightRadius:25
    },
    caption:{
        height:'auto',
        flex:0,
        borderTopRightRadius:25
    },
    detail:{
        borderRadius:25,
        borderBottomRightRadius:0
    },
    captionText:{
        fontWeight:'bold'
    },
    detailContent:{
        marginLeft:10
    },
    iconSize:{
        width:50,
        height:50
    },
    datePickerSize:{
        height:'auto'
    },
    baseFlex:{
        flex:1
    },
    rowDirection:{
        flexDirection:'row'
    },
    actionIconSize:{
        width:25,
        height:25,
    }
})