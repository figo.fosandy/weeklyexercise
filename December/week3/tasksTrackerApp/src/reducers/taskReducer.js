import {GET_TASK_LIST_STARTED,GET_TASK_LIST_SUCCESS,GET_TASK_LIST_FAILURE} from '../actions/type'

const initialState={
    newLoading:false,
    newList:[],
    newError:null,
    'in-progressLoading':false,
    'in-progressList':[],
    'in-progressError':null,
    finishLoading:false,
    finishList:[],
    finishError:null
}

export const taskReducer=(state=initialState,action)=>{
    switch(action.type){
        case GET_TASK_LIST_STARTED:
            return{
                ...state,
                [`${action.payload.status}Loading`]:true
            }
        case GET_TASK_LIST_SUCCESS:
            return{
                ...state,
                [`${action.payload.status}Loading`]:false,
                [`${action.payload.status}Error`]:null,
                [`${action.payload.status}List`]:action.payload.result
            }
        case GET_TASK_LIST_STARTED:
            return{
                ...state,
                [`${action.payload.status}loading`]:false,
                [`${action.payload.status}Error`]:action.payload.error
            }            
        default:
            return state
    }
}