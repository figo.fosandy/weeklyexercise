import {combineReducers} from 'redux'
import {taskReducer} from './taskReducer'

const appReducers=combineReducers({
    task:taskReducer
})

const rootReducer=(state,action)=>{
    return appReducers(state,action)
}

export default rootReducer