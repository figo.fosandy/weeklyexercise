import {GET_TASK_LIST_STARTED,GET_TASK_LIST_SUCCESS,GET_TASK_LIST_FAILURE} from './type'
import axios from 'axios'

const getTaskListStarted=(status)=>{
    return {
        type:GET_TASK_LIST_STARTED,
        payload:{
            status
        }
    }
}

const getTaskListSuccess=(status,tasks)=>{
    return {
        type:GET_TASK_LIST_SUCCESS,
        payload:{
            result:tasks,
            status
        }
    }
}

const getTaskListFailure=(status,error)=>{
    return {
        type:GET_TASK_LIST_FAILURE,
        payload:{
            error,
            status
        }
    }
}

const getTaskList=(date,status)=>{
    return dispatch=>{
        dispatch(getTaskListStarted(status))
        axios
        .get(`http://192.168.204.2:1207/tasks?dueDate=${date}&filter[status]=${status}`,{
            auth:{
                username:'user',
                password:'password'
            }
        })
        .then(res=>{
            dispatch(getTaskListSuccess(status,res.data))
        })
        .catch(err=>{
            dispatch(getTaskListFailure(status,err))
        })
    }
}

const actionTask=(id,action,dueDate)=>{
    return dispatch=>{
        axios
            .post(`http://192.168.204.2:1207/tasks/${id}/${action}`,{},{
                auth:{
                    username:'user',
                    password:'password'
                }
            })
            .then(()=>{
                dispatch(getTaskList(dueDate,'new'))
                dispatch(getTaskList(dueDate,'in-progress'))
                dispatch(getTaskList(dueDate,'finish'))
            })
            .catch(err=>{
                dispatch(getTaskListFailure(err))
            })
    }
}

const updateTask=(id,payload,dueDate)=>{
    return dispatch=>{
        axios
            .put(`http://192.168.204.2:1207/tasks/${id}`,payload,{
                auth:{
                    username:'user',
                    password:'password'
                }
            })
            .then(()=>{
                dispatch(getTaskList(dueDate,'new'))
                dispatch(getTaskList(dueDate,'in-progress'))
                dispatch(getTaskList(dueDate,'finish'))
            })
            .catch(err=>{
                dispatch(getTaskListFailure(err))
            })
    }
}

const addTask=(payload,dueDate)=>{
    return dispatch=>{
        axios
            .post(`http://192.168.204.2:1207/tasks`,payload,{
                auth:{
                    username:'user',
                    password:'password'
                }
            })
            .then(()=>{
                dispatch(getTaskList(dueDate,'new'))
                dispatch(getTaskList(dueDate,'in-progress'))
                dispatch(getTaskList(dueDate,'finish'))
            })
            .catch(err=>{
                dispatch(getTaskListFailure(err))
            })
    }
}

export {getTaskList,actionTask,updateTask,addTask}