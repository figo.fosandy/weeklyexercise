# Excercise This Week

## Task Tracker App
I create it on directory [taskTrackerApp]
Excercise this week is creating App for tracking Tasks using React Native and should be :
- See a list of tasks due today
- Filter by date to see tasks due tomorrow (or any selected date)
- Add a new task, with a due date
- Edit a task. Add comment. Change status (new to in-progress, in-progress to complete)

### Approach
I using 'react-navigation-stack' for moving between screens, 
and 'redux' for manage state for all components, and 'redux-thunk' middleware
for allowing updating state asynchronously

### Step to run the code
First, u can download this code as [zip] or [tar]
Next :
```sh
$ cd December/week3/tasksTrackerApp
$ npm i
$ npm start
$ npm run android
```
### Problem
Cleaning Code, naming, and maybe just that

#### Notes
- Sorry, i replace complete->finish
- Home Screen is TaksList displaying all task by dueDate
- In Home Screen, there is display 3 types of tasks: new, in-progress, & finish
- In Home Screen, u can only display specifics types of task with press the type-text
- In Home Screen, u can change dueDate with press the date
- In Home Screen, u can move to the detail for editing or start/finish with press the list
- In Home Screen, u can move to the create screen with press the add button
- Detail Screen is TaskDetail displaying detail, and for create/update/run the task
- In Detail Screen, u can change the value of field, by clicking the value
- In Detail Screen, u can start/finish the task, on right bottom side


   [zip]:<https://gitlab.com/figo.fosandy/weeklyexercise/-/archive/master/weeklyexercise-master.zip>
   [tar]:<https://gitlab.com/figo.fosandy/weeklyexercise/-/archive/master/weeklyexercise-master.tar.gz>
   [taskTrackerApp]:<https://gitlab.com/figo.fosandy/weeklyexercise/tree/master/December/week3/tasksTrackerApp>